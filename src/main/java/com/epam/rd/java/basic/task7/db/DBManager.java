package com.epam.rd.java.basic.task7.db;

import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DBManager {

    private static Connection connection;

    static {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection(Files.readString(Path.of("app.properties")).replace("connection.url=", ""));
        } catch (SQLException | ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
    }

    public static synchronized DBManager getInstance() {
        return new DBManager();
    }

    public List<User> findAllUsers() throws DBException {
        List<User> list = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM users");
            while (resultSet.next())
                list.add(new User(resultSet.getInt(1), resultSet.getString(2)));
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        return list;
    }

    public boolean insertUser(User user) throws DBException {
        try {
            connection.createStatement().executeUpdate("INSERT INTO users VALUES(DEFAULT, '" + user.getLogin() + "')");
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT id FROM users WHERE login = '" + user.getLogin() + "'");
            while (resultSet.next())
                user.setId(resultSet.getInt(1));
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        return true;
    }

    public boolean deleteUsers(User... users) throws DBException {
        for (User user : users) {
            try {
                connection.createStatement().executeUpdate("DELETE FROM users WHERE login = '" + user.getLogin() + "'");
            } catch (SQLException e) {
                throw new DBException(e.getMessage(), e);
            }
        }
        return true;
    }

    public User getUser(String login) throws DBException {
        return User.createUser(login);
    }

    public Team getTeam(String name) throws DBException {
        return Team.createTeam(name);
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> list = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM teams");
            while (resultSet.next())
                list.add(new Team(resultSet.getInt(1), resultSet.getString(2)));
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        return list;
    }

    public boolean insertTeam(Team team) throws DBException {
        try {
            connection.createStatement().executeUpdate("INSERT INTO teams VALUES(DEFAULT, '" + team.getName() + "')");
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT id FROM teams WHERE name = '" + team.getName() + "'");
            while (resultSet.next())
                team.setId(resultSet.getInt(1));
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        return true;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        try {
            System.out.println(connection.getAutoCommit());
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        try {
            connection.commit();
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        try {
            for (Team team : teams) {
                connection.createStatement().executeUpdate("INSERT INTO users_teams VALUES(" + user.getId() + ", " + team.getId() + ")");
            }
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                throw new DBException(e.getMessage(), e);
            }
            System.out.println(e.getMessage());
            if(e.getMessage().split("'")[0].equals("The statement was aborted because it would have caused a duplicate " +
                    "key value in a unique or primary key constraint or unique index identified by "))
                throw new DBException(e.getMessage(), e);
        }
        try {
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        return true;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> list = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM users_teams WHERE user_id = " + user.getId());
            while (resultSet.next()) {
                Statement statement1 = connection.createStatement();
                ResultSet resultSet1 = statement1.executeQuery("SELECT * FROM teams WHERE id = " + resultSet.getInt(2));
                while (resultSet1.next())
                    list.add(new Team(resultSet1.getInt(1), resultSet1.getString(2)));
            }

        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        return list;
    }

    public boolean deleteTeam(Team team) throws DBException {
        try {
            connection.createStatement().executeUpdate("DELETE FROM teams WHERE id = " + team.getId());
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        return true;
    }

    public boolean updateTeam(Team team) throws DBException {
        try {
            connection.createStatement().executeUpdate("UPDATE teams SET name = '" + team.getName() + "'WHERE id = " + team.getId());
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        return true;
    }

}
